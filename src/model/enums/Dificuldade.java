package model.enums;

public enum Dificuldade {

	INICIANTE,
	INTERMEDIÁRIO,
	AVANÇADO;
}
