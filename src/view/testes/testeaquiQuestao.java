package view.testes;

import model.dao.DaoFactory;
import model.dao.QuestaoDao;
import model.entities.Curso;
import model.entities.Questao;
import model.entities.Tarefa;

public class testeaquiQuestao {
	public static void main(String[] args) {
		
		QuestaoDao questao = DaoFactory.createQuestaoDao();
		
//		System.out.println("TESTE1 - INSERT QUESTAO");
//		questao.insert(new Questao(null, "Como se chama a linguagem que será usada? ", null, "b", new Tarefa("Atividade1", null, null, null)));
		
//		System.out.println("TESTE2 - DELETE QUESTAO");
//		questao.deleteById(5);
		
//		System.out.println("TESTE3 - UPDATE QUESTAO");
//		questao.update(new Questao(4, "Qual a capital de São Paulo?", null, "a", new Tarefa("#1", null, null, null)));
		
//		System.out.println("TESTE4 - FINDBYID QUESTAO");
//		System.out.println(questao.findById(4));
		
		System.out.println("TESTE5 - FINDALL QUESTAO");
		System.out.println(questao.findAll());
	}
}
