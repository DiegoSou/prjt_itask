package control.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ConexaoDb {
	
	private static Connection conn = null;

	public static Connection getConn() {
		return conn;
	}
	
	public static Connection abrirConexao() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Properties props = carregarProps();
			String url = props.getProperty("dburl");
			conn = DriverManager.getConnection(url, props);
			return conn;
		}
		catch (Exception e) {
			throw new ConexaoException(e.getMessage());
		}
	}
	
	public static boolean fecharConexao() {
		try {
			conn.close();
			return true;
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
	}
	
	public static Properties carregarProps() throws IOException{
		FileInputStream fs = new FileInputStream("db.properties");
		Properties props = new Properties();
		props.load(fs);
		return props;
	}
	
	public static void fecharStatement(Statement st) {
		try {
			st.close();
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
	}
	
	public static void fecharResultSet(ResultSet rs) {
		try {
			rs.close();
		}
		catch (SQLException e) {
			throw new ConexaoException(e.getMessage());
		}
	}
}
